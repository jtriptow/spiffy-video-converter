﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spiffy_Video_Converter
{
    public partial class log : Form
    {
        public log()
        {
            InitializeComponent();
            logStatus.Text = "";
        }
        public void updateLog(string message, string status)
        {
            if (status == "STATUS")
            {
                logStatus.Text = message;
            }
            else
            {
                statusBox.SelectionStart = statusBox.Text.Length;
                if (status == "error")
                {
                    statusBox.SelectionColor = Color.Red;
                    message = "[Error] " + message;
                }
                else if (status == "warning")
                {
                    statusBox.SelectionColor = Color.OrangeRed;
                    message = "[Warning] " + message;
                }
                else if (status == "info")
                {
                    statusBox.SelectionColor = Color.Black;
                    message = "[Info] " + message;
                }
                else if (status == "done")
                {
                    statusBox.SelectionColor = Color.DarkGreen;
                    message = "[Success] " + message;
                }
                else
                {
                    statusBox.SelectionColor = Color.Black;
                }
                if (statusBox.Text == "")
                {
                    //statusLog.Text = message;
                    statusBox.AppendText(message);
                }
                else
                {
                    if (message == "LINEBREAK")
                    {
                        statusBox.AppendText(System.Environment.NewLine + "---------------------------------------------------------------------------------------------------");
                    }
                    else
                    {
                        statusBox.AppendText(System.Environment.NewLine + message);
                    }
                }
                statusBox.SelectionStart = statusBox.Text.Length;
                statusBox.ScrollToCaret();
            }
        }
        private void log_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
            }
        }

        private void clearLog_Click(object sender, EventArgs e)
        {
            statusBox.Text = "";
        }
    }
}
