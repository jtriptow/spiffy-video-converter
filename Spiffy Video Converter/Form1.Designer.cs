﻿namespace Spiffy_Video_Converter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.themeInfo = new System.Windows.Forms.Label();
            this.statusBox = new System.Windows.Forms.RichTextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.convertVideos = new System.Windows.Forms.Button();
            this.deleteOriginal = new System.Windows.Forms.CheckBox();
            this.videoQ = new System.Windows.Forms.DataGridView();
            this.Video = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConvertType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Flipped = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.VideoPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Settings = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.removeSelected = new System.Windows.Forms.Button();
            this.convertTo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabber = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.remaining = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.fps = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btn_advanced = new System.Windows.Forms.Button();
            this.advancedOptions = new System.Windows.Forms.GroupBox();
            this.skipPs4 = new System.Windows.Forms.CheckBox();
            this.ps4size = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.command = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.enableAudioMap = new System.Windows.Forms.CheckBox();
            this.squarecrop = new System.Windows.Forms.CheckBox();
            this.audioMap = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.sampleClip = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.offsetInput = new System.Windows.Forms.TextBox();
            this.cancelConvert = new System.Windows.Forms.Button();
            this.buttonTop = new System.Windows.Forms.Button();
            this.buttonBottom = new System.Windows.Forms.Button();
            this.buttonDown = new System.Windows.Forms.Button();
            this.buttonUp = new System.Windows.Forms.Button();
            this.originalDir = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.removeIndex = new System.Windows.Forms.CheckBox();
            this.pendingDownloads = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.newtoold = new System.Windows.Forms.CheckBox();
            this.cancelDownload = new System.Windows.Forms.Button();
            this.template = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.playlistIndexE = new System.Windows.Forms.NumericUpDown();
            this.playlistIndexS = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.statusBox2 = new System.Windows.Forms.RichTextBox();
            this.doUpdate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.youtube = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.videoOption = new System.Windows.Forms.ListBox();
            this.convertVideo = new System.Windows.Forms.Button();
            this.downloadOption = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.removeJoin = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.joinOutName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.joinVids = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.joinVideos = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.debug = new System.Windows.Forms.CheckBox();
            this.updateapp = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Import = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.videoQ)).BeginInit();
            this.tabber.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.advancedOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.audioMap)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playlistIndexE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playlistIndexS)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.joinVids)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // themeInfo
            // 
            this.themeInfo.AutoSize = true;
            this.themeInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.themeInfo.Location = new System.Drawing.Point(3, 11);
            this.themeInfo.Name = "themeInfo";
            this.themeInfo.Size = new System.Drawing.Size(157, 16);
            this.themeInfo.TabIndex = 72;
            this.themeInfo.Text = "Drag / drop your video(s)";
            // 
            // statusBox
            // 
            this.statusBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusBox.BackColor = System.Drawing.Color.White;
            this.statusBox.Location = new System.Drawing.Point(236, 11);
            this.statusBox.Name = "statusBox";
            this.statusBox.ReadOnly = true;
            this.statusBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.statusBox.Size = new System.Drawing.Size(384, 254);
            this.statusBox.TabIndex = 99;
            this.statusBox.Text = "";
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(3, 623);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(629, 28);
            this.progressBar1.TabIndex = 107;
            // 
            // convertVideos
            // 
            this.convertVideos.BackColor = System.Drawing.Color.Green;
            this.convertVideos.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.convertVideos.Location = new System.Drawing.Point(31, 195);
            this.convertVideos.Name = "convertVideos";
            this.convertVideos.Size = new System.Drawing.Size(186, 56);
            this.convertVideos.TabIndex = 108;
            this.convertVideos.Text = "Convert Videos";
            this.convertVideos.UseVisualStyleBackColor = false;
            this.convertVideos.Click += new System.EventHandler(this.btn_convetVideo);
            // 
            // deleteOriginal
            // 
            this.deleteOriginal.AutoSize = true;
            this.deleteOriginal.Location = new System.Drawing.Point(6, 50);
            this.deleteOriginal.Name = "deleteOriginal";
            this.deleteOriginal.Size = new System.Drawing.Size(136, 17);
            this.deleteOriginal.TabIndex = 111;
            this.deleteOriginal.Text = "Delete Original Video(s)";
            this.deleteOriginal.UseVisualStyleBackColor = true;
            // 
            // videoQ
            // 
            this.videoQ.AllowUserToAddRows = false;
            this.videoQ.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.videoQ.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.videoQ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.videoQ.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Video,
            this.ConvertType,
            this.Flipped,
            this.VideoPath,
            this.Settings});
            this.videoQ.Location = new System.Drawing.Point(3, 271);
            this.videoQ.Name = "videoQ";
            this.videoQ.RowHeadersVisible = false;
            this.videoQ.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.videoQ.Size = new System.Drawing.Size(595, 323);
            this.videoQ.TabIndex = 112;
            // 
            // Video
            // 
            this.Video.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Video.FillWeight = 54.0146F;
            this.Video.HeaderText = "Video";
            this.Video.Name = "Video";
            this.Video.ReadOnly = true;
            this.Video.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // ConvertType
            // 
            this.ConvertType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ConvertType.FillWeight = 145.9854F;
            this.ConvertType.HeaderText = "Convert To";
            this.ConvertType.Name = "ConvertType";
            this.ConvertType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ConvertType.Width = 130;
            // 
            // Flipped
            // 
            this.Flipped.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Flipped.HeaderText = "Flip Video";
            this.Flipped.Name = "Flipped";
            this.Flipped.Width = 60;
            // 
            // VideoPath
            // 
            this.VideoPath.HeaderText = "Video Path";
            this.VideoPath.Name = "VideoPath";
            this.VideoPath.Visible = false;
            // 
            // Settings
            // 
            this.Settings.HeaderText = "Settings";
            this.Settings.Name = "Settings";
            this.Settings.Visible = false;
            // 
            // removeSelected
            // 
            this.removeSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.removeSelected.BackColor = System.Drawing.Color.Maroon;
            this.removeSelected.Enabled = false;
            this.removeSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeSelected.ForeColor = System.Drawing.Color.White;
            this.removeSelected.Location = new System.Drawing.Point(601, 497);
            this.removeSelected.Name = "removeSelected";
            this.removeSelected.Size = new System.Drawing.Size(34, 33);
            this.removeSelected.TabIndex = 113;
            this.removeSelected.Text = "X";
            this.removeSelected.UseVisualStyleBackColor = false;
            this.removeSelected.Click += new System.EventHandler(this.btn_removeVideos);
            // 
            // convertTo
            // 
            this.convertTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.convertTo.FormattingEnabled = true;
            this.convertTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.convertTo.Items.AddRange(new object[] {
            "RC Plane MP4",
            "Xbox AVI",
            "Blu-ray - Convert",
            "Blu-ray - Copy",
            "InnoTab Divx",
            "InnoTab H.264",
            "InnoTab Divx +3 vol",
            "InnoTab H.264 +3 vol"});
            this.convertTo.Location = new System.Drawing.Point(86, 119);
            this.convertTo.Name = "convertTo";
            this.convertTo.Size = new System.Drawing.Size(144, 21);
            this.convertTo.TabIndex = 114;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 16);
            this.label1.TabIndex = 115;
            this.label1.Text = "Convert To:";
            // 
            // tabber
            // 
            this.tabber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabber.Controls.Add(this.tabPage1);
            this.tabber.Controls.Add(this.tabPage2);
            this.tabber.Controls.Add(this.tabPage3);
            this.tabber.Controls.Add(this.tabPage4);
            this.tabber.Location = new System.Drawing.Point(12, 12);
            this.tabber.Name = "tabber";
            this.tabber.SelectedIndex = 0;
            this.tabber.Size = new System.Drawing.Size(643, 680);
            this.tabber.TabIndex = 116;
            // 
            // tabPage1
            // 
            this.tabPage1.AllowDrop = true;
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.remaining);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.fps);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.btn_advanced);
            this.tabPage1.Controls.Add(this.advancedOptions);
            this.tabPage1.Controls.Add(this.cancelConvert);
            this.tabPage1.Controls.Add(this.buttonTop);
            this.tabPage1.Controls.Add(this.buttonBottom);
            this.tabPage1.Controls.Add(this.buttonDown);
            this.tabPage1.Controls.Add(this.buttonUp);
            this.tabPage1.Controls.Add(this.originalDir);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.themeInfo);
            this.tabPage1.Controls.Add(this.convertTo);
            this.tabPage1.Controls.Add(this.statusBox);
            this.tabPage1.Controls.Add(this.removeSelected);
            this.tabPage1.Controls.Add(this.progressBar1);
            this.tabPage1.Controls.Add(this.videoQ);
            this.tabPage1.Controls.Add(this.convertVideos);
            this.tabPage1.Controls.Add(this.deleteOriginal);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(635, 654);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Convert Videos";
            this.tabPage1.DragDrop += new System.Windows.Forms.DragEventHandler(this.video_dragDrop);
            this.tabPage1.DragEnter += new System.Windows.Forms.DragEventHandler(this.video_dragEnter);
            // 
            // remaining
            // 
            this.remaining.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.remaining.AutoSize = true;
            this.remaining.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remaining.Location = new System.Drawing.Point(84, 601);
            this.remaining.Name = "remaining";
            this.remaining.Size = new System.Drawing.Size(15, 16);
            this.remaining.TabIndex = 138;
            this.remaining.Text = "0";
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 601);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 16);
            this.label16.TabIndex = 137;
            this.label16.Text = "Remaining:";
            // 
            // fps
            // 
            this.fps.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.fps.AutoSize = true;
            this.fps.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fps.Location = new System.Drawing.Point(600, 601);
            this.fps.Name = "fps";
            this.fps.Size = new System.Drawing.Size(15, 16);
            this.fps.TabIndex = 136;
            this.fps.Text = "0";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(563, 601);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 16);
            this.label15.TabIndex = 135;
            this.label15.Text = "FPS";
            // 
            // btn_advanced
            // 
            this.btn_advanced.Location = new System.Drawing.Point(9, 166);
            this.btn_advanced.Name = "btn_advanced";
            this.btn_advanced.Size = new System.Drawing.Size(109, 23);
            this.btn_advanced.TabIndex = 126;
            this.btn_advanced.Text = "Advanced Options";
            this.btn_advanced.UseVisualStyleBackColor = true;
            this.btn_advanced.Click += new System.EventHandler(this.btn_advanced_Click);
            // 
            // advancedOptions
            // 
            this.advancedOptions.Controls.Add(this.skipPs4);
            this.advancedOptions.Controls.Add(this.ps4size);
            this.advancedOptions.Controls.Add(this.label14);
            this.advancedOptions.Controls.Add(this.command);
            this.advancedOptions.Controls.Add(this.label13);
            this.advancedOptions.Controls.Add(this.enableAudioMap);
            this.advancedOptions.Controls.Add(this.squarecrop);
            this.advancedOptions.Controls.Add(this.audioMap);
            this.advancedOptions.Controls.Add(this.label10);
            this.advancedOptions.Controls.Add(this.sampleClip);
            this.advancedOptions.Controls.Add(this.label9);
            this.advancedOptions.Controls.Add(this.offsetInput);
            this.advancedOptions.Location = new System.Drawing.Point(96, 336);
            this.advancedOptions.Name = "advancedOptions";
            this.advancedOptions.Size = new System.Drawing.Size(426, 221);
            this.advancedOptions.TabIndex = 125;
            this.advancedOptions.TabStop = false;
            this.advancedOptions.Text = "Advanced Options";
            this.advancedOptions.Visible = false;
            // 
            // skipPs4
            // 
            this.skipPs4.AutoSize = true;
            this.skipPs4.Checked = true;
            this.skipPs4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.skipPs4.Location = new System.Drawing.Point(208, 139);
            this.skipPs4.Name = "skipPs4";
            this.skipPs4.Size = new System.Drawing.Size(204, 17);
            this.skipPs4.TabIndex = 135;
            this.skipPs4.Text = "Skip videos under width above  (PS4)";
            this.skipPs4.UseVisualStyleBackColor = true;
            // 
            // ps4size
            // 
            this.ps4size.Location = new System.Drawing.Point(314, 113);
            this.ps4size.Name = "ps4size";
            this.ps4size.Size = new System.Drawing.Size(92, 20);
            this.ps4size.TabIndex = 134;
            this.ps4size.Text = "1800";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(246, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 16);
            this.label14.TabIndex = 133;
            this.label14.Text = "PS4 Size:";
            // 
            // command
            // 
            this.command.Location = new System.Drawing.Point(97, 178);
            this.command.Name = "command";
            this.command.Size = new System.Drawing.Size(309, 20);
            this.command.TabIndex = 132;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(16, 178);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 16);
            this.label13.TabIndex = 131;
            this.label13.Text = "Command:";
            // 
            // enableAudioMap
            // 
            this.enableAudioMap.AutoSize = true;
            this.enableAudioMap.Location = new System.Drawing.Point(18, 113);
            this.enableAudioMap.Name = "enableAudioMap";
            this.enableAudioMap.Size = new System.Drawing.Size(113, 17);
            this.enableAudioMap.TabIndex = 130;
            this.enableAudioMap.Text = "Enable Audio Map";
            this.enableAudioMap.UseVisualStyleBackColor = true;
            // 
            // squarecrop
            // 
            this.squarecrop.AutoSize = true;
            this.squarecrop.Checked = true;
            this.squarecrop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.squarecrop.Location = new System.Drawing.Point(240, 30);
            this.squarecrop.Name = "squarecrop";
            this.squarecrop.Size = new System.Drawing.Size(166, 17);
            this.squarecrop.TabIndex = 128;
            this.squarecrop.Text = "Crop to square (Smart Watch)";
            this.squarecrop.UseVisualStyleBackColor = true;
            // 
            // audioMap
            // 
            this.audioMap.Location = new System.Drawing.Point(97, 136);
            this.audioMap.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.audioMap.Name = "audioMap";
            this.audioMap.Size = new System.Drawing.Size(91, 20);
            this.audioMap.TabIndex = 127;
            this.audioMap.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 136);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 16);
            this.label10.TabIndex = 125;
            this.label10.Text = "Audio Map:";
            // 
            // sampleClip
            // 
            this.sampleClip.AutoSize = true;
            this.sampleClip.Location = new System.Drawing.Point(18, 30);
            this.sampleClip.Name = "sampleClip";
            this.sampleClip.Size = new System.Drawing.Size(155, 17);
            this.sampleClip.TabIndex = 124;
            this.sampleClip.Text = "Create a sample clip (5 min)";
            this.sampleClip.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(16, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 16);
            this.label9.TabIndex = 121;
            this.label9.Text = "Offset:";
            // 
            // offsetInput
            // 
            this.offsetInput.Location = new System.Drawing.Point(67, 67);
            this.offsetInput.Name = "offsetInput";
            this.offsetInput.Size = new System.Drawing.Size(121, 20);
            this.offsetInput.TabIndex = 122;
            this.offsetInput.Text = "00:00:00.0";
            // 
            // cancelConvert
            // 
            this.cancelConvert.BackColor = System.Drawing.Color.Firebrick;
            this.cancelConvert.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelConvert.Location = new System.Drawing.Point(31, 195);
            this.cancelConvert.Name = "cancelConvert";
            this.cancelConvert.Size = new System.Drawing.Size(186, 56);
            this.cancelConvert.TabIndex = 123;
            this.cancelConvert.Text = "Cancel";
            this.cancelConvert.UseVisualStyleBackColor = false;
            this.cancelConvert.Visible = false;
            this.cancelConvert.Click += new System.EventHandler(this.btn_cancelConvert);
            // 
            // buttonTop
            // 
            this.buttonTop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTop.Location = new System.Drawing.Point(601, 294);
            this.buttonTop.Name = "buttonTop";
            this.buttonTop.Size = new System.Drawing.Size(34, 33);
            this.buttonTop.TabIndex = 120;
            this.buttonTop.Text = "▲";
            this.buttonTop.UseVisualStyleBackColor = true;
            this.buttonTop.Click += new System.EventHandler(this.btnTop_Click);
            // 
            // buttonBottom
            // 
            this.buttonBottom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBottom.Location = new System.Drawing.Point(601, 411);
            this.buttonBottom.Name = "buttonBottom";
            this.buttonBottom.Size = new System.Drawing.Size(34, 33);
            this.buttonBottom.TabIndex = 119;
            this.buttonBottom.Text = "▼";
            this.buttonBottom.UseVisualStyleBackColor = true;
            this.buttonBottom.Click += new System.EventHandler(this.btnBottom_Click);
            // 
            // buttonDown
            // 
            this.buttonDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDown.Location = new System.Drawing.Point(601, 372);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(34, 33);
            this.buttonDown.TabIndex = 118;
            this.buttonDown.Text = "↓";
            this.buttonDown.UseVisualStyleBackColor = true;
            this.buttonDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // buttonUp
            // 
            this.buttonUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUp.Location = new System.Drawing.Point(601, 333);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(34, 33);
            this.buttonUp.TabIndex = 117;
            this.buttonUp.Text = "↑";
            this.buttonUp.UseVisualStyleBackColor = true;
            this.buttonUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // originalDir
            // 
            this.originalDir.AutoSize = true;
            this.originalDir.Checked = true;
            this.originalDir.CheckState = System.Windows.Forms.CheckState.Checked;
            this.originalDir.Location = new System.Drawing.Point(6, 73);
            this.originalDir.Name = "originalDir";
            this.originalDir.Size = new System.Drawing.Size(154, 17);
            this.originalDir.TabIndex = 116;
            this.originalDir.Text = "Convert to original directory";
            this.originalDir.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Transparent;
            this.tabPage2.ContextMenuStrip = this.contextMenuStrip1;
            this.tabPage2.Controls.Add(this.Import);
            this.tabPage2.Controls.Add(this.removeIndex);
            this.tabPage2.Controls.Add(this.pendingDownloads);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.newtoold);
            this.tabPage2.Controls.Add(this.cancelDownload);
            this.tabPage2.Controls.Add(this.template);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.playlistIndexE);
            this.tabPage2.Controls.Add(this.playlistIndexS);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.statusBox2);
            this.tabPage2.Controls.Add(this.doUpdate);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.youtube);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.progressBar2);
            this.tabPage2.Controls.Add(this.videoOption);
            this.tabPage2.Controls.Add(this.convertVideo);
            this.tabPage2.Controls.Add(this.downloadOption);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(635, 654);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Video Downloader";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(144, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(143, 22);
            this.toolStripMenuItem1.Text = "Paste";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.btn_paste);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(143, 22);
            this.toolStripMenuItem2.Text = "Paste and Go";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.btn_pasteGo);
            // 
            // removeIndex
            // 
            this.removeIndex.AutoSize = true;
            this.removeIndex.Checked = true;
            this.removeIndex.CheckState = System.Windows.Forms.CheckState.Checked;
            this.removeIndex.Location = new System.Drawing.Point(362, 186);
            this.removeIndex.Name = "removeIndex";
            this.removeIndex.Size = new System.Drawing.Size(143, 17);
            this.removeIndex.TabIndex = 140;
            this.removeIndex.Text = "Remove Index from URL";
            this.removeIndex.UseVisualStyleBackColor = true;
            // 
            // pendingDownloads
            // 
            this.pendingDownloads.AutoSize = true;
            this.pendingDownloads.Location = new System.Drawing.Point(179, 11);
            this.pendingDownloads.Name = "pendingDownloads";
            this.pendingDownloads.Size = new System.Drawing.Size(51, 13);
            this.pendingDownloads.TabIndex = 139;
            this.pendingDownloads.Text = "Queue: 0";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(394, 6);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(92, 23);
            this.button5.TabIndex = 138;
            this.button5.Text = "Open Location";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.openDownloadLocation);
            // 
            // newtoold
            // 
            this.newtoold.AutoSize = true;
            this.newtoold.Checked = true;
            this.newtoold.CheckState = System.Windows.Forms.CheckState.Checked;
            this.newtoold.Location = new System.Drawing.Point(362, 163);
            this.newtoold.Name = "newtoold";
            this.newtoold.Size = new System.Drawing.Size(79, 17);
            this.newtoold.TabIndex = 137;
            this.newtoold.Text = "New to Old";
            this.newtoold.UseVisualStyleBackColor = true;
            // 
            // cancelDownload
            // 
            this.cancelDownload.BackColor = System.Drawing.Color.Firebrick;
            this.cancelDownload.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelDownload.Location = new System.Drawing.Point(526, 154);
            this.cancelDownload.Name = "cancelDownload";
            this.cancelDownload.Size = new System.Drawing.Size(98, 40);
            this.cancelDownload.TabIndex = 135;
            this.cancelDownload.Text = "Cancel";
            this.cancelDownload.UseVisualStyleBackColor = false;
            this.cancelDownload.Visible = false;
            this.cancelDownload.Click += new System.EventHandler(this.btn_cancelDownload);
            // 
            // template
            // 
            this.template.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.template.FormattingEnabled = true;
            this.template.Items.AddRange(new object[] {
            "Default",
            "Playlist"});
            this.template.Location = new System.Drawing.Point(182, 184);
            this.template.Name = "template";
            this.template.Size = new System.Drawing.Size(121, 21);
            this.template.TabIndex = 134;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 20);
            this.label6.TabIndex = 133;
            this.label6.Text = "File Save Template:";
            // 
            // playlistIndexE
            // 
            this.playlistIndexE.Location = new System.Drawing.Point(362, 131);
            this.playlistIndexE.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.playlistIndexE.Name = "playlistIndexE";
            this.playlistIndexE.Size = new System.Drawing.Size(120, 20);
            this.playlistIndexE.TabIndex = 132;
            // 
            // playlistIndexS
            // 
            this.playlistIndexS.Location = new System.Drawing.Point(362, 101);
            this.playlistIndexS.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.playlistIndexS.Name = "playlistIndexS";
            this.playlistIndexS.Size = new System.Drawing.Size(120, 20);
            this.playlistIndexS.TabIndex = 131;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(358, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 20);
            this.label3.TabIndex = 130;
            this.label3.Text = "Playlist start/end index:";
            // 
            // statusBox2
            // 
            this.statusBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusBox2.BackColor = System.Drawing.Color.White;
            this.statusBox2.ContextMenuStrip = this.contextMenuStrip1;
            this.statusBox2.Location = new System.Drawing.Point(10, 208);
            this.statusBox2.Name = "statusBox2";
            this.statusBox2.ReadOnly = true;
            this.statusBox2.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.statusBox2.Size = new System.Drawing.Size(619, 411);
            this.statusBox2.TabIndex = 129;
            this.statusBox2.Text = "";
            // 
            // doUpdate
            // 
            this.doUpdate.Location = new System.Drawing.Point(573, 6);
            this.doUpdate.Name = "doUpdate";
            this.doUpdate.Size = new System.Drawing.Size(56, 23);
            this.doUpdate.TabIndex = 128;
            this.doUpdate.Text = "update";
            this.doUpdate.UseVisualStyleBackColor = true;
            this.doUpdate.Click += new System.EventHandler(this.doUpdate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 20);
            this.label5.TabIndex = 119;
            this.label5.Text = "Enter the video URL:";
            // 
            // youtube
            // 
            this.youtube.ContextMenuStrip = this.contextMenuStrip1;
            this.youtube.Location = new System.Drawing.Point(10, 35);
            this.youtube.Name = "youtube";
            this.youtube.Size = new System.Drawing.Size(619, 20);
            this.youtube.TabIndex = 120;
            this.youtube.TextChanged += new System.EventHandler(this.checkUrlForPlaylist);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(178, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 20);
            this.label2.TabIndex = 126;
            this.label2.Text = "Video Options:";
            // 
            // progressBar2
            // 
            this.progressBar2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar2.Location = new System.Drawing.Point(10, 625);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(619, 23);
            this.progressBar2.TabIndex = 121;
            // 
            // videoOption
            // 
            this.videoOption.FormattingEnabled = true;
            this.videoOption.Items.AddRange(new object[] {
            "Default",
            "Highest quality available (4k)",
            "Highest quality mp4 (4k)",
            "Best quality mp4",
            "Mid quality mp4"});
            this.videoOption.Location = new System.Drawing.Point(182, 101);
            this.videoOption.Name = "videoOption";
            this.videoOption.Size = new System.Drawing.Size(161, 69);
            this.videoOption.TabIndex = 125;
            // 
            // convertVideo
            // 
            this.convertVideo.BackColor = System.Drawing.Color.LightGray;
            this.convertVideo.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.convertVideo.Location = new System.Drawing.Point(534, 103);
            this.convertVideo.Name = "convertVideo";
            this.convertVideo.Size = new System.Drawing.Size(82, 48);
            this.convertVideo.TabIndex = 122;
            this.convertVideo.Text = "GO";
            this.convertVideo.UseVisualStyleBackColor = false;
            this.convertVideo.Click += new System.EventHandler(this.btn_downloadVideo);
            // 
            // downloadOption
            // 
            this.downloadOption.FormattingEnabled = true;
            this.downloadOption.Items.AddRange(new object[] {
            "Video",
            "Video + mp3",
            "mp3"});
            this.downloadOption.Location = new System.Drawing.Point(10, 102);
            this.downloadOption.Name = "downloadOption";
            this.downloadOption.Size = new System.Drawing.Size(146, 69);
            this.downloadOption.TabIndex = 124;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 20);
            this.label4.TabIndex = 123;
            this.label4.Text = "Download Option:";
            // 
            // tabPage3
            // 
            this.tabPage3.AllowDrop = true;
            this.tabPage3.BackColor = System.Drawing.Color.Transparent;
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.removeJoin);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.joinOutName);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.joinVids);
            this.tabPage3.Controls.Add(this.joinVideos);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(635, 654);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Join Videos";
            this.tabPage3.DragDrop += new System.Windows.Forms.DragEventHandler(this.video_dragDrop2);
            this.tabPage3.DragEnter += new System.Windows.Forms.DragEventHandler(this.video_dragEnter2);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(595, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 33);
            this.button1.TabIndex = 127;
            this.button1.Text = "▲";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnTopJoin_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(595, 245);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(34, 33);
            this.button2.TabIndex = 126;
            this.button2.Text = "▼";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnBottomJoin_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(595, 206);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(34, 33);
            this.button3.TabIndex = 125;
            this.button3.Text = "↓";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.btnDownJoin_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(595, 167);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(34, 33);
            this.button4.TabIndex = 124;
            this.button4.Text = "↑";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.btnUpJoin_Click);
            // 
            // removeJoin
            // 
            this.removeJoin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.removeJoin.BackColor = System.Drawing.Color.Maroon;
            this.removeJoin.Enabled = false;
            this.removeJoin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeJoin.ForeColor = System.Drawing.Color.White;
            this.removeJoin.Location = new System.Drawing.Point(595, 331);
            this.removeJoin.Name = "removeJoin";
            this.removeJoin.Size = new System.Drawing.Size(34, 33);
            this.removeJoin.TabIndex = 123;
            this.removeJoin.Text = "X";
            this.removeJoin.UseVisualStyleBackColor = false;
            this.removeJoin.Click += new System.EventHandler(this.btn_removeJoin);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(7, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 16);
            this.label8.TabIndex = 122;
            this.label8.Text = "Output Filename:";
            // 
            // joinOutName
            // 
            this.joinOutName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.joinOutName.Location = new System.Drawing.Point(121, 42);
            this.joinOutName.Name = "joinOutName";
            this.joinOutName.Size = new System.Drawing.Size(282, 20);
            this.joinOutName.TabIndex = 121;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(7, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(157, 16);
            this.label7.TabIndex = 117;
            this.label7.Text = "Drag / drop your video(s)";
            // 
            // joinVids
            // 
            this.joinVids.AllowUserToAddRows = false;
            this.joinVids.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.joinVids.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.joinVids.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.joinVids.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.joinVids.Location = new System.Drawing.Point(7, 101);
            this.joinVids.Name = "joinVids";
            this.joinVids.RowHeadersVisible = false;
            this.joinVids.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.joinVids.Size = new System.Drawing.Size(585, 547);
            this.joinVids.TabIndex = 115;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.FillWeight = 54.0146F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Video";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Video Path";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // joinVideos
            // 
            this.joinVideos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.joinVideos.BackColor = System.Drawing.Color.Green;
            this.joinVideos.Enabled = false;
            this.joinVideos.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.joinVideos.Location = new System.Drawing.Point(443, 6);
            this.joinVideos.Name = "joinVideos";
            this.joinVideos.Size = new System.Drawing.Size(186, 56);
            this.joinVideos.TabIndex = 114;
            this.joinVideos.Text = "Join Videos";
            this.joinVideos.UseVisualStyleBackColor = false;
            this.joinVideos.Click += new System.EventHandler(this.btn_joinVideo);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.debug);
            this.tabPage4.Controls.Add(this.updateapp);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.pictureBox1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(635, 654);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "About";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // debug
            // 
            this.debug.AccessibleName = "debug";
            this.debug.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.debug.AutoSize = true;
            this.debug.Location = new System.Drawing.Point(506, 6);
            this.debug.Name = "debug";
            this.debug.Size = new System.Drawing.Size(123, 17);
            this.debug.TabIndex = 136;
            this.debug.Text = "Show Debug Output";
            this.debug.UseVisualStyleBackColor = true;
            this.debug.CheckedChanged += new System.EventHandler(this.toggleDebug);
            // 
            // updateapp
            // 
            this.updateapp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.updateapp.Location = new System.Drawing.Point(554, 29);
            this.updateapp.Name = "updateapp";
            this.updateapp.Size = new System.Drawing.Size(75, 23);
            this.updateapp.TabIndex = 3;
            this.updateapp.Text = "Update";
            this.updateapp.UseVisualStyleBackColor = true;
            this.updateapp.Click += new System.EventHandler(this.updateLatest);
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label12.AutoSize = true;
            this.label12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label12.Font = new System.Drawing.Font("GrilledCheese BTN Wide Blk", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 482);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(595, 44);
            this.label12.TabIndex = 2;
            this.label12.Text = "http://SpiffyHacks.com";
            this.label12.Click += new System.EventHandler(this.loadSpiffyHacks);
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("GrilledCheese BTN Wide Blk", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 384);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(580, 44);
            this.label11.TabIndex = 1;
            this.label11.Text = "Created by Deak Phreak";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Spiffy_Video_Converter.Properties.Resources.logo;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(57, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(503, 322);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.loadSpiffyHacks);
            // 
            // Import
            // 
            this.Import.Location = new System.Drawing.Point(492, 6);
            this.Import.Name = "Import";
            this.Import.Size = new System.Drawing.Size(75, 23);
            this.Import.TabIndex = 141;
            this.Import.Text = "Import";
            this.Import.UseVisualStyleBackColor = true;
            this.Import.Click += new System.EventHandler(this.btn_downloadFile);
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(667, 704);
            this.Controls.Add(this.tabber);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(683, 742);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.videoQ)).EndInit();
            this.tabber.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.advancedOptions.ResumeLayout(false);
            this.advancedOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.audioMap)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playlistIndexE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playlistIndexS)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.joinVids)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label themeInfo;
        private System.Windows.Forms.RichTextBox statusBox;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button convertVideos;
        private System.Windows.Forms.CheckBox deleteOriginal;
        private System.Windows.Forms.DataGridView videoQ;
        private System.Windows.Forms.Button removeSelected;
        private System.Windows.Forms.ComboBox convertTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabber;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RichTextBox statusBox2;
        private System.Windows.Forms.Button doUpdate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox youtube;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.ListBox videoOption;
        private System.Windows.Forms.Button convertVideo;
        private System.Windows.Forms.ListBox downloadOption;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox originalDir;
        private System.Windows.Forms.NumericUpDown playlistIndexS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown playlistIndexE;
        private System.Windows.Forms.ComboBox template;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView joinVids;
        private System.Windows.Forms.Button joinVideos;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox joinOutName;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Button buttonTop;
        private System.Windows.Forms.Button buttonBottom;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button removeJoin;
        private System.Windows.Forms.TextBox offsetInput;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button cancelConvert;
        private System.Windows.Forms.Button cancelDownload;
        private System.Windows.Forms.CheckBox newtoold;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label pendingDownloads;
        private System.Windows.Forms.CheckBox sampleClip;
        private System.Windows.Forms.GroupBox advancedOptions;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_advanced;
        private System.Windows.Forms.NumericUpDown audioMap;
        private System.Windows.Forms.CheckBox squarecrop;
        private System.Windows.Forms.CheckBox enableAudioMap;
        private System.Windows.Forms.DataGridViewTextBoxColumn Video;
        private System.Windows.Forms.DataGridViewComboBoxColumn ConvertType;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Flipped;
        private System.Windows.Forms.DataGridViewTextBoxColumn VideoPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn Settings;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox command;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox ps4size;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label fps;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label remaining;
        private System.Windows.Forms.Button updateapp;
        public System.Windows.Forms.CheckBox debug;
        private System.Windows.Forms.CheckBox skipPs4;
        private System.Windows.Forms.CheckBox removeIndex;
        private System.Windows.Forms.Button Import;
    }
}

