﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Taskbar;
using System.Text.RegularExpressions;


namespace Spiffy_Video_Converter
{
    public partial class Form1 : Form
    {
        string Version = "4.0.6";
        string videoFolder = "ConvertedVideos";
        string cacheFolder = "cache";
        double videoDuration = 0;
        double totalFrames = 0;
        bool hasError = false;
        bool isRunning = false;
        bool isRunning2 = false;
        bool skipMapping = false;
        List<string> files = null;
        List<string> fileData = new List<string>();
        List<string> audioData = new List<string>();
        List<string> downloadQueue = new List<string>();

        bool extractAudio = false;
        bool runUpdate = false;
        bool hasError2 = false;
        bool convertCanceled = false;
        bool convertCanceled2 = false;
        bool isAdded = false;
        bool skipPS4 = false;
        bool skipQueue = false;
        string convertprofile = "";
        string maxps4res = "";
        string ps4Size = "";
        string downOption = "";
        string args = "";
        string onlyfile = "";
        string vidIn = "";
        string vidOut = "";
        char slash = Path.DirectorySeparatorChar;
        string curpath = Directory.GetCurrentDirectory();
        //string appfile = "Spiffy Video Converter.exe";
        string appfile = "Spiffy Video Converter.exe";
        string updatepath = "";
        string videoFolder2 = "Downloaded";
        string fullSaveFile = "";
        string fullDownloadFile = "";
        string queueFile = "";
        string downFile = "";
        string logFile = "";
        int procId1 = 0;
        int procId2 = 0;
        BackgroundWorker bw = new BackgroundWorker();
        BackgroundWorker bw2 = new BackgroundWorker();
        BackgroundWorker bw3 = new BackgroundWorker();
        log debugwindow = new log();

        //private TaskDialog td = null;
        private TaskbarManager windowsTaskbar = TaskbarManager.Instance;


        string[] convertOptions = { "RC Plane MP4", 
                             "Quad Copter",
                             "Xbox AVI", 
                             "Xbox MP4 Animated", 
                             "Xbox MP4 General", 
                             "PS4", 
                             "PS4 alt", 
                             "PS4 Remove 3D SBS",
                             "PS4 Remove 3D TB",
                             "Blu-ray - Convert", 
                             "Blu-ray - Copy", 
                             "Blu-ray - Cabin", 
                             "Canon Camcorder MTS", 
                             "Smart Watch",
                             "Remove 3D SBS",
                             "Remove 3D TB",
                             "Straight Convert to mp4" };



        public Form1()
        {
            InitializeComponent();

            Text = "Spiffy Video Converter v" + Version;
            downloadOption.SelectedIndex = 0;
            videoOption.SelectedIndex = 0;
            convertVideo.Enabled = false;

            videoOption.SelectedIndex = 4;
            template.SelectedIndex = 0;
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += new DoWorkEventHandler(bw_DoConvert);
            bw.ProgressChanged += new ProgressChangedEventHandler(bw_ConvertChanged);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_ConvertCompleted);

            bw2.WorkerReportsProgress = true;
            bw2.WorkerSupportsCancellation = true;
            bw2.DoWork += new DoWorkEventHandler(bw2_DoDownload);
            bw2.ProgressChanged += new ProgressChangedEventHandler(bw2_DownloadChanged);
            bw2.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw2_DownloadCompleted);

            bw3.WorkerReportsProgress = true;
            bw3.WorkerSupportsCancellation = true;
            bw3.DoWork += new DoWorkEventHandler(getVideoInfo);
            //bw3.ProgressChanged += new ProgressChangedEventHandler(bw2_DownloadChanged);
            bw3.RunWorkerCompleted += new RunWorkerCompletedEventHandler(getVideoCompleted);

            //buttonDown.Text = char.ConvertFromUtf32(8595);
            //buttonUp.Text = char.ConvertFromUtf32(8593);

            debugwindow.FormClosing += new FormClosingEventHandler(log_FormClosed);

            this.convertTo.DataSource = convertOptions;
            ConvertType.DataSource = convertOptions;
            convertTo.SelectedIndex = 9;
            if (checkCacheFolder())
            {
                if (checkFfmpeg())
                {
                }
            }
            checkFolder();

            queueFile = Path.Combine(curpath, cacheFolder) + slash + "queue.txt";
            downFile = Path.Combine(curpath, cacheFolder) + slash + "import.txt";
            logFile = Path.Combine(curpath, cacheFolder) + slash + "log.txt";

            updatepath = "E:\\My Apps" + slash + "Sources" + slash + "Spiffy Video Converter" + slash + "Spiffy Video Converter" + slash + "bin" + slash + "Debug" + slash;

            if (File.Exists(queueFile))
            {
                //MessageBox.Show("There is an old downloader queue file!");
                if (MessageBox.Show("Do you want to finish downloading the files?", "Old queue file found!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    downloadQueueFile();
                }
            }
            if (File.Exists(appfile + ".OLD"))
            {
                System.IO.File.Delete(appfile + ".OLD");
            }
        }



        // Video Converter
        private void btn_removeVideos(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to remove these?", "Spiffy Video Converter",
               MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                foreach (DataGridViewRow r in videoQ.SelectedRows)
                {
                    if (isRunning && r.Index == 0)
                    {
                        UpdateLog("You are unable to remove a video that is processing", "warning");
                    }
                    else
                    {
                        videoQ.Rows.Remove(r);
                    }
                }
            }
        }
        private void btn_convetVideo(object sender, EventArgs e)
        {
            if (checkCacheFolder() && checkFfmpeg())
            {
                //tabber.Enabled = false;
                if (videoQ.Rows.Count > 0)
                {
                    convertVideos.Visible = false;
                    cancelConvert.Visible = true;
                    run();
                }
                else
                {
                    UpdateLog("No videos pending", "error");
                }
            }
        }
        private void btn_cancelConvert(object sender, EventArgs e)
        {
            if (isRunning)
            {
                convertCanceled = true;
                bw.CancelAsync();
                bw.Dispose();
                //UpdateLog("Kill PID: " + procId1, "info");
                Process p = Process.GetProcessById(procId1);
                p.Kill();
                procId1 = 0;
                System.Threading.Thread.Sleep(2000);
                File.Delete(fullSaveFile);
                ResetForm();
            }
        }
        private void run()
        {
            hasError = false;
            isRunning = true;
            string file = videoQ.Rows[0].Cells[3].Value.ToString();
            string convertSettings = videoQ.Rows[0].Cells[4].Value.ToString();
            string ext = Path.GetExtension(file).ToLower();
            bool isFlipped = (Boolean)videoQ.Rows[0].Cells[2].Value;
            string convertType = videoQ.Rows[0].Cells[1].Value.ToString();
            string fliparg = "";
            string scalecrop = "";
            string saveTo = "";
            string offsetarg = "";
            string endargs = "-threads 0";
            string saveExt = "mp4";
            string audioMapping = "";
            if (!skipMapping)
            {
                audioMapping = "-map 0:0 -map 0:" + getConvertOptions(4, convertSettings);
            }
            else
            {
                skipMapping = false;
            }

            string offset = getConvertOptions(3, convertSettings);

            vidIn = file;
            vidOut = GetSafeFilename(Path.GetFileNameWithoutExtension(file)).Trim();

            if (ext == ".vob")
            {
                audioMapping = "";
            }
            if (getConvertOptions(1, convertSettings) == "1")
            {
                saveTo = Path.GetDirectoryName(vidIn);
            }
            else
            {
                saveTo = videoFolder;
            }
            if (getConvertOptions(2, convertSettings) == "1")
            {
                endargs = endargs + " -t 5:00";
            }


            if (isFlipped)
            {
                fliparg = "-vf \"vflip,hflip\"";
            }
            if (offset != "00:00:00.0")
            {
                offsetarg = "-itsoffset " + offset + " -i \"" + vidIn + "\"";
            }
            if (convertType == "Xbox AVI" || convertType == "Smart Watch")
            {
                saveExt = "avi";
            }
            if (File.Exists(saveTo + "\\" + vidOut + "." + saveExt))
            {
                vidOut += " - recoded";
            }
            if (command.Text != "")
            {
                //UpdateLog("USE CUSTOM COMMAND: " + command.Text, "info");
                args = "-y -i \"" + vidIn + "\" " + command.Text + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
            }
            else
            {
                switch (convertType)
                {
                    //case "PS4":
                    //    args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -an -vcodec libx264 -profile:v high -level:v 4.2 -filter:v scale=1800:trunc(ow/a/2)*2 -preset slow -crf 18 -c:a libvo_aacenc -q:a 100 -ac 2 " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                    //    break;
                    case "Remove 3D SBS" :
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -vf \"crop=w=iw/2:h=ih:x=0:y=0,scale=w=2*iw:h=ih,setdar=2\" " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "Remove 3D TB":
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -vf \"crop=w=iw:h=ih/2:x=0:y=0,scale=w=iw:h=ih/2,setdar=2\" " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "PS4":
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -map 0:0 -map 0:1 -c:v libx264 -profile:v high -r 23.967 -crf 18 -level 4.2 -preset slow -tune film -filter:v scale=" + ps4size.Text + ":trunc(ow/a/2)*2 -sws_flags lanczos -c:a libvo_aacenc -q:a 100 -ac 2 -map_metadata -1 " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "PS4 alt":
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -map 0:0 -map 0:1 -c:v libx264 -profile:v baseline -r 23.967 -crf 24 -level 3.0 -preset slow -tune film -filter:v scale=" + ps4size.Text + ":trunc(ow/a/2)*2 -sws_flags lanczos -c:a libvo_aacenc -q:a 100 -ac 2 -map_metadata -1 " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "PS4 Remove 3D SBS":
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -map 0:0 -map 0:1 -c:v libx264 -profile:v high -r 23.967 -crf 18 -level 4.2 -preset slow -tune film -vf \"crop=w=iw/2:h=ih:x=0:y=0,scale=" + ps4size.Text + ":ow/2,setdar=2\" -sws_flags lanczos -c:a libvo_aacenc -q:a 100 -ac 2 -map_metadata -1 " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "PS4 Remove 3D TB":
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -map 0:0 -map 0:1 -c:v libx264 -profile:v high -r 23.967 -crf 18 -level 4.2 -preset slow -tune film -vf \"crop=w=iw:h=ih/2:x=0:y=0,scale=" + ps4size.Text + ":ow/2,setdar=2\" -sws_flags lanczos -c:a libvo_aacenc -q:a 100 -ac 2 -map_metadata -1 " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "RC Plane MP4":
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "Quad Copter":
                        //ffmpeg -i MOVI0001.avi -vcodec libx264 -profile:v baseline -pix_fmt yuv420p -af volume=0.5 -map_metadata -1 outputtest.mp4
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -vcodec libx264 -profile:v baseline -pix_fmt yuv420p -af volume=0.3 -map_metadata -1 " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "Xbox AVI":
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -c:v mpeg4 -vtag xvid -b:v 2000k -acodec libmp3lame -ac 2 -ar 48000 -ab 128k -vf scale=\"'if(gt(a,4/3),640,-1)':'if(gt(a,4/3),-1,272)'\" " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "Xbox MP4 Animated":
                        if (offset != "00:00:00.0")
                        {
                            args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -c:v libx264 -profile:v baseline -r 23.967 -crf 25 -level 3.0 -preset slow -tune animation -filter:v scale=640:trunc(ow/a/2)*2 -sws_flags lanczos -c:a libvo_aacenc -q:a 100 -ac 2 " + endargs + " -map_metadata -1 \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        }
                        else
                        {
                            args = "-y -i \"" + vidIn + "\" " + fliparg + " " + audioMapping + " -c:v libx264 -profile:v baseline -r 23.967 -crf 25 -level 3.0 -preset slow -tune animation -filter:v scale=640:trunc(ow/a/2)*2 -sws_flags lanczos -c:a libvo_aacenc -q:a 100 -ac 2 " + endargs + " -map_metadata -1 \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        }
                        break;
                    case "Xbox MP4 General":
                        if (ext == ".dat")
                        {
                            args = "-y -i \"" + vidIn + "\" " + fliparg + " " + audioMapping + " -c:v libx264 -profile:v baseline -r 23.967 -crf 24 -level 3.0 -preset slow -tune film -filter:v scale=640:trunc(ow/a/2)*2 -sws_flags lanczos " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        }
                        else
                        {
                            if (offset != "00:00:00.0")
                            {
                                args = "-y " + offsetarg + " -i \"" + vidIn + "\" " + fliparg + " " + audioMapping + " -c:v libx264 -profile:v baseline -r 23.967 -crf 24 -level 3.0 -preset slow -tune film -filter:v scale=640:trunc(ow/a/2)*2 -sws_flags lanczos -c:a libvo_aacenc -q:a 100 -ac 2 " + endargs + " -map_metadata -1 \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                            }
                            else
                            {
                                args = "-y -i \"" + vidIn + "\" " + fliparg + " " + audioMapping + " -c:v libx264 -profile:v baseline -r 23.967 -crf 24 -level 3.0 -preset slow -tune film -filter:v scale=640:trunc(ow/a/2)*2 -sws_flags lanczos -c:a libvo_aacenc -q:a 100 -ac 2 " + endargs + " -map_metadata -1 \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                            }
                        }
                        break;
                    case "Blu-ray - Convert":
                        //args = "-y -i \"" + vidIn + "\" -vcodec copy -acodec ac3 \"" + videoFolder + "\\" + vidOut + ".mp4\"";
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -c:v h264 -q:v 9 -acodec ac3 -ab 448k " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "Blu-ray - Cabin":
                        //args = "-y -i \"" + vidIn + "\" -vcodec copy -c:a libvo_aacenc -q:a 100 -ac 2 -threads 0 \"" + saveTo + "\\" + vidOut + ".mp4\"";
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -c:v h264 -q:v 9 -c:a libvo_aacenc -q:a 100 -ac 2 " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "Blu-ray - Copy":
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -vcodec copy -acodec ac3 " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "Canon Camcorder MTS":
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " -vf yadif=1 -s 1280x720 -r 60 -vcodec mpeg4 -b:v 15M " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "Smart Watch":

                        if (squarecrop.Checked)
                        {
                            //scalecrop = "\"scale='if(gt(a,4/3),240,-1)':'if(gt(a,4/3),-1,240)' , crop=min(iw\\,ih*(1/1)):ow/(1/1)\" -aspect 1:1";
                            scalecrop = "\"crop=min(iw\\,ih*(1/1)):ow/(1/1) , scale='if(gt(a,4/3),240,-1)':'if(gt(a,4/3),-1,240)'\" -aspect 1:1";
                            //scalecrop = "\"crop=min(iw\\,ih*(4/3)):ow/(4/3) , scale='if(gt(a,4/3),240,-1)':'if(gt(a,4/3),-1,240)'\" -aspect 4:3";
                        }
                        else
                        {
                            scalecrop = "scale=\"'if(gt(a,4/3),240,-1)':'if(gt(a,4/3),-1,240)'\"";
                        }
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -c:v mjpeg -qscale 15 -acodec libmp3lame -ac 1 -vf " + scalecrop + " " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";

                        //args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " -c:v mjpeg -qscale 15 -acodec libmp3lame -ac 1 -vf scale=\"'if(gt(a,4/3),240,-1)':'if(gt(a,4/3),-1,240)'\" " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                    case "Straight Convert to mp4":
                    default:
                        args = "-y -i \"" + vidIn + "\" " + fliparg + " " + offsetarg + " " + audioMapping + " " + endargs + " \"" + saveTo + "\\" + vidOut + "." + saveExt + "\"";
                        break;
                }
            }

            UpdateLog("Command: ffmpeg.exe " + args, "info", true);
            fullSaveFile = saveTo + "\\" + vidOut + "." + saveExt;
            videoQ.Rows[0].ReadOnly = true;
            UpdateLog("Converting '" + vidOut + "'", "info");
            if (ext == ".dat")
            {
                UpdateLog("This file type does not support a progress bar", "warning");
                UpdateLog("Please wait until the conversion is finished", "warning");
            }
            if (File.Exists(vidIn))
            {
                bw.RunWorkerAsync();
            }
            else
            {
                UpdateLog("File not found!", "error");
                videoQ.Rows.RemoveAt(0);
                if (videoQ.Rows.Count > 0)
                {
                    run();
                }
                else
                {
                    UpdateLog("LINEBREAK", "");
                    ResetForm();
                }
            }
        }
        private void getVideoInfo(object sender, DoWorkEventArgs e)
        {
            isAdded = false;
            skipPS4 = false;
            if (files.Count > 0)
            {
                string f = files[0];
                try
                {
                    foreach (DataGridViewRow row in videoQ.Rows)
                    {
                        if (row.Cells[3].Value.ToString().Equals(f))
                        {
                            isAdded = true;
                            break;
                        }
                    }
                }
                catch (Exception exc)
                {
                    //MessageBox.Show(exc.Message);
                    UpdateLog("Error checking video: '" + exc.ToString() + "'", "error");
                }

                if (!isAdded)
                {
                    Process pro = new Process();
                    pro.StartInfo.RedirectStandardOutput = true;
                    pro.StartInfo.RedirectStandardError = true;
                    pro.StartInfo.UseShellExecute = false;
                    pro.StartInfo.CreateNoWindow = true;
                    pro.EnableRaisingEvents = true;
                    pro.OutputDataReceived += new DataReceivedEventHandler(ParseVideoData);
                    pro.ErrorDataReceived += new DataReceivedEventHandler(ParseVideoData);

                    pro.StartInfo.FileName = "ffmpeg.exe";
                    pro.StartInfo.Arguments = "-i \"" + f + "\"";

                    try
                    {
                        pro.Start();
                        //procId3 = pro.Id;
                        pro.BeginOutputReadLine();
                        pro.BeginErrorReadLine();
                        pro.WaitForExit();
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.ToString());
                        UpdateLog("Error getting video info: '" + ex.ToString() + "'", "error");
                    }

                }
                else
                {
                    UpdateLog("Video has already been added '" + onlyfile + "'", "warning");
                }
            }
        }
        private void bw_DoConvert(object sender, DoWorkEventArgs e)
        {
            Process pro = new Process();
            pro.StartInfo.RedirectStandardOutput = true;
            pro.StartInfo.RedirectStandardError = true;
            pro.StartInfo.UseShellExecute = false;
            pro.StartInfo.CreateNoWindow = true;
            pro.EnableRaisingEvents = true;
            pro.OutputDataReceived += new DataReceivedEventHandler(ParseFFmpegData);
            pro.ErrorDataReceived += new DataReceivedEventHandler(ParseFFmpegData);

            pro.StartInfo.FileName = "ffmpeg.exe";
            pro.StartInfo.Arguments = args;

            try
            {
                pro.Start();
                procId1 = pro.Id;
                //UpdateLog("Started ffmpeg process Id procId1 = " + procId1, "info");
                pro.BeginOutputReadLine();
                pro.BeginErrorReadLine();
                pro.WaitForExit();
            }

            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                UpdateLog("Error starting conversion: '" + ex.ToString() + "'", "error");
            }
        }
        void bw_ConvertChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage > 100)
            {
                progressBar1.Value = 100;
                //TaskbarManager.Instance.SetProgressValue(100, 100);
                doTaskbarProgress(100, 0);
            }
            else if (e.ProgressPercentage < 0)
            {
                progressBar1.Value = 0;
                //TaskbarManager.Instance.SetProgressValue(0, 100);
                doTaskbarProgress(0, 0);
            }
            else
            {
                progressBar1.Value = e.ProgressPercentage;
                //TaskbarManager.Instance.SetProgressValue(e.ProgressPercentage, 100);
                doTaskbarProgress(e.ProgressPercentage, 0);
            }
        }
        void bw_ConvertCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            UpdateConvertData("0","");
            UpdateLog("", "STATUS", true);
            TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.NoProgress);
            if (convertCanceled)
            {
                UpdateLog("Conversion Canceled", "info");
                convertCanceled = false;
            }
            else
            {
                if ((e.Cancelled == true))
                {
                    UpdateLog("Conversion Canceled", "info");
                    File.Delete(fullSaveFile);
                    ResetForm();
                }
                else if (!(e.Error == null))
                {
                    UpdateLog("Conversion Error: " + e.Error.Message, "error");
                }
                else
                {
                    if (hasError)
                    {
                        UpdateLog("Looks like we may have had a conversion error", "error");
                        progressBar1.Value = 0;
                    }
                    else
                    {
                        progressBar1.Value = 0;
                        if (getConvertOptions(0, videoQ.Rows[0].Cells[4].Value.ToString()) == "1")
                        {
                            if (File.Exists(fullSaveFile))
                            {
                                long length = new System.IO.FileInfo(fullSaveFile).Length;
                                if (length > 1000) // ensure we have a decent sized file
                                {
                                    File.Delete(@vidIn);
                                    UpdateLog("Video converted and original deleted", "done");
                                }
                                else
                                {
                                    UpdateLog("We may have had an issue. Old file not deleted", "warning");
                                }
                                //UpdateLog("File Size: " + length.ToString(), "info");
                            }
                            else
                            {
                                UpdateLog("Converted file not found, we may have had an error converting", "warning");
                                if (MessageBox.Show("Do you want to try converting without data mapping?", "Converted file not found",
                                   MessageBoxButtons.YesNo) == DialogResult.Yes)
                                {
                                    skipMapping = true;
                                    run();
                                    return;
                                }
                                else
                                {
                                    
                                }
                            }
                        }
                        else
                        {
                            UpdateLog("Video converted", "done");
                        }
                    }
                }
                videoQ.Rows.RemoveAt(0);
                if (videoQ.Rows.Count > 0)
                {
                    run();
                }
                else
                {
                    UpdateLog("LINEBREAK", "");
                    ResetForm();
                }
            }
        }
        void getVideoCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!isAdded && !skipPS4)
            {
                string f = files[0];
                onlyfile = Path.GetFileName(f);
                videoQ.Rows.Add(new object[] { onlyfile, convertTo.Text, false, f, createConvertOptions() });
                UpdateLog("Added video " + f, "info", true);
            }
            if (skipPS4)
            {
                UpdateLog("Skipped video because it was less than or equal to " + ps4size.Text, "warning");
                skipPS4 = false;
            }
            audioData.Clear();
            files.RemoveAt(0);
            if (files.Count > 0)
            {
                bw3.RunWorkerAsync();
            }
            else
            {
                if (!isRunning)
                {
                    progressBar1.Value = 0;
                    progressBar1.Style = ProgressBarStyle.Continuous;
                    windowsTaskbar.SetProgressState(TaskbarProgressBarState.Normal);
                    TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.NoProgress);
                    //windowsTaskbar.SetProgressValue(0, 100);
                }
            }
            convertVideos.Enabled = true;
            removeSelected.Enabled = true;

        }
        public void ParseFFmpegData(object sender, DataReceivedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Data))
            {
                if (e.Data.Contains("Duration:"))
                {
                    string videoDurationTmp = e.Data.Substring(e.Data.IndexOf("Duration: ") + ("Duration: ").Length, ("00:00:00.00").Length);
                    
                    if (!videoDurationTmp.Trim().StartsWith("N/A"))
                    {
                        videoDuration = Math.Round(TimeSpan.Parse(videoDurationTmp).TotalSeconds, 0);
                        UpdateLog("Video Duration: " + videoDuration + " seconds", "info", true);
                    }
                }
                if (e.Data.Contains("Stream #") && e.Data.Contains("tbr"))
                {
                    //videoFps = e.Data;
                    var regex = new Regex(@"((?:\d*\.)?\d+) (tbr)");
                    var fpsTemp = regex.Matches(e.Data);
                    string tempdata = fpsTemp[0].ToString();
                    double videoFps = Convert.ToDouble(tempdata.Replace(" tbr", ""));

                    totalFrames = Math.Round(videoDuration * videoFps);
                    UpdateLog("Video Frames: " + totalFrames, "info", true);

                    //string fpsTemp = e.Data.Substring(e.Data.IndexOf("fps") + 5);
                    //UpdateLog("totalFrames Data: " + totalFrames, "warning");
                }
                else if (e.Data.Contains("Stream #") && e.Data.Contains("fps"))
                {
                    var regex = new Regex(@"((?:\d*\.)?\d+) (fps)");
                    var fpsTemp = regex.Matches(e.Data);
                    string tempdata = fpsTemp[0].ToString();
                    double videoFps = Convert.ToDouble(tempdata.Replace(" fps", ""));

                    totalFrames = Math.Round(videoDuration * videoFps);
                }
                if (e.Data.Contains("frame="))
                {
                    if (videoDuration > 0)
                    {
                        try
                        {
                            //UpdateLog("output: " + e.Data, "info");
                            string str = e.Data.Substring(e.Data.IndexOf("fps=") + 4);
                            string frames = str.Remove(str.IndexOf("q") - 1).Trim();

                            str = e.Data.Substring(e.Data.IndexOf("frame=") + 6);
                            string curFrame = str.Remove(str.IndexOf("fps") - 1).Trim();
                            string frameDrop = "0";
                            if (e.Data.Contains("drop="))
                            {
                                frameDrop = e.Data.Substring(e.Data.IndexOf("drop=")+5).Trim();
                                //UpdateLog("frameDrop: " + frameDrop, "info");
                            }
                            string frameDup = "0";
                            if (e.Data.Contains("dup="))
                            {
                                string frameDupTmp = e.Data.Substring(e.Data.IndexOf("dup=") + 4).Trim();
                                frameDup = frameDupTmp.Remove(frameDupTmp.IndexOf("drop=") - 1).Trim();
                                //UpdateLog("frameDup: " + frameDup, "info");
                            }
                            double totalCurFrames = Convert.ToDouble(curFrame) + Convert.ToDouble(frameDrop) + Convert.ToDouble(frameDup);
                            //UpdateLog("totalCurFrames: " + totalCurFrames, "info");

                            if(totalCurFrames > totalFrames) {
                                totalCurFrames = totalFrames;
                            }

                            UpdateLog("Frame: " + totalCurFrames + " / " + totalFrames, "STATUS", true);

                            string timeRemaining = "0";
                            if (totalFrames != 0 && totalCurFrames != 0 && frames != "0" && frames != "0.0")
                            {
                                TimeSpan secondsRemaining = TimeSpan.FromSeconds(Math.Round((totalFrames - totalCurFrames) / Convert.ToDouble(frames)));
                                timeRemaining = secondsRemaining.ToString(@"hh\:mm\:ss");
                            }

                            //timeRemaining = timeRemaining + " -- Frame: " + totalCurFrames + " out of " + totalFrames;

                            UpdateConvertData(frames, timeRemaining);



                            //UpdateLog("fps: " + frames, "info");
                            str = e.Data.Substring(e.Data.IndexOf(":") - 2);
                            int percentComplete = Convert.ToInt32(TimeSpan.Parse(str.Remove(str.IndexOf("b") - 1)).TotalSeconds / videoDuration * 100);
                            bw.ReportProgress(percentComplete);
                        }
                        catch (Exception exc)
                        {
                            UpdateLog(exc.Message, "error", true);
                        }
                    }
                    //UpdateLog(e.Data, "info");
                }
                if (e.Data.Contains("Error"))
                {
                    UpdateLog(e.Data, "error");
                    hasError = true;
                }

            }
        }
        public void ParseVideoData(object sender, DataReceivedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Data))
            {
                if (e.Data.Contains("Stream #") && e.Data.Contains("Audio:"))
                {
                    UpdateLog("Audio Data: " + e.Data, "info", true);
                    audioData.Add(e.Data);
                }
                if (e.Data.Contains("Stream #") && e.Data.Contains("Video:") && !skipPS4)
                {
                    UpdateLog("Video Data: " + e.Data, "info", true);
                    if (skipPs4.Checked && isPs4Converting(convertprofile))
                    {
                        try
                        {
                            //var regex = new Regex(@"(\d+)x(\d+)");
                            var regex = new Regex(@"(\d+){2}x(\d+){2}");
                            var vidres = regex.Matches(e.Data);
                            ps4Size = vidres[0].ToString().Trim();

                            string[] split = ps4Size.Split('x');
                            if (Convert.ToInt32(split[0]) <= Convert.ToInt32(maxps4res))
                            {
                                UpdateLog("Video width is " + split[0] + " Max PS4 width is " + maxps4res, "info", true);
                                skipPS4 = true;
                            }
                            UpdateLog("Video Res: " + ps4Size, "info", true);
                        }
                        catch (Exception ex)
                        {
                            UpdateLog(ex.Message, "error", true);
                        }
                    }
                }
            }
        }
        private void video_dragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                if (!isRunning)
                {
                    windowsTaskbar.SetProgressState(TaskbarProgressBarState.Indeterminate);
                    progressBar1.Style = ProgressBarStyle.Marquee;
                    progressBar1.MarqueeAnimationSpeed = 5;
                }
                string[] temp = (string[])e.Data.GetData(DataFormats.FileDrop);
                files = new List<string>(temp);
                convertprofile = convertTo.Text;
                maxps4res = ps4size.Text;
                bw3.RunWorkerAsync();
                /*
                bool isAdded = false;

                if (files.Count > 0)
                {
                    foreach (string f in files)
                    {
                        isAdded = false;

                        try
                        {
                            foreach (DataGridViewRow row in videoQ.Rows)
                            {
                                if (row.Cells[3].Value.ToString().Equals(f))
                                {
                                    isAdded = true;
                                    break;
                                }
                            }
                        }
                        catch (Exception exc)
                        {
                            //MessageBox.Show(exc.Message);
                        }

                        if (!isAdded)
                        {
                            onlyfile = Path.GetFileName(f);
                            // Get video info
                            UpdateLog("Getting video info '" + f + "'", "warning");
                            bw3.RunWorkerAsync(f);

                            while (!gotVideoData)
                            {
                                if (!bw3.IsBusy)
                                {
                                    break;
                                }
                            }
                            
                            gotVideoData = false;
                            videoQ.Rows.Add(new object[] { onlyfile, convertTo.Text, false, f, createConvertOptions() });
                        }
                        else
                        {
                            UpdateLog("Video has already been added '" + onlyfile + "'", "warning");
                        }
                    }
                    convertVideos.Enabled = true;
                    removeSelected.Enabled = true;
                }
                */
            }
        }
        private void video_dragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        private void btnUp_Click(object sender, EventArgs e)
        {
            DataGridView grid = videoQ;
            try
            {
                int totalRows = grid.Rows.Count;
                int idx = grid.SelectedCells[0].OwningRow.Index;
                if (idx == 0 || (isRunning && idx == 1))
                    return;
                int col = grid.SelectedCells[0].OwningColumn.Index;
                DataGridViewRowCollection rows = grid.Rows;
                DataGridViewRow row = rows[idx];
                rows.Remove(row);
                rows.Insert(idx - 1, row);
                grid.ClearSelection();
                grid.Rows[idx - 1].Cells[col].Selected = true;

            }
            catch { }
        }
        private void btnDown_Click(object sender, EventArgs e)
        {

            DataGridView grid = videoQ;
            try
            {
                int totalRows = grid.Rows.Count;
                int idx = grid.SelectedCells[0].OwningRow.Index;
                if (idx == totalRows - 1 || (isRunning && idx == 0))
                    return;
                int col = grid.SelectedCells[0].OwningColumn.Index;
                DataGridViewRowCollection rows = grid.Rows;
                DataGridViewRow row = rows[idx];
                rows.Remove(row);
                rows.Insert(idx + 1, row);
                grid.ClearSelection();
                grid.Rows[idx + 1].Cells[col].Selected = true;
            }
            catch { }
        }
        private void btnTop_Click(object sender, EventArgs e)
        {
            DataGridView grid = videoQ;
            try
            {
                int upIndex = 0;
                int totalRows = grid.Rows.Count;
                int idx = grid.SelectedCells[0].OwningRow.Index;
                if (idx == 0 || (isRunning && idx == 1))
                    return;
                int col = grid.SelectedCells[0].OwningColumn.Index;
                DataGridViewRowCollection rows = grid.Rows;
                DataGridViewRow row = rows[idx];
                rows.Remove(row);

                if (isRunning)
                {
                    upIndex = 1;
                }

                rows.Insert(upIndex, row);
                grid.ClearSelection();
                grid.Rows[upIndex].Cells[col].Selected = true;
            }
            catch { }
        }
        private void btnBottom_Click(object sender, EventArgs e)
        {
            DataGridView grid = videoQ;
            try
            {
                int totalRows = grid.Rows.Count;
                int idx = grid.SelectedCells[0].OwningRow.Index;
                if (idx == totalRows - 1 || (isRunning && idx == 0))
                    return;
                int col = grid.SelectedCells[0].OwningColumn.Index;
                DataGridViewRowCollection rows = grid.Rows;
                DataGridViewRow row = rows[idx];
                rows.Remove(row);
                rows.Insert(totalRows - 1, row);
                grid.ClearSelection();
                grid.Rows[totalRows - 1].Cells[col].Selected = true;
            }
            catch { }
        }
        private void btn_advanced_Click(object sender, EventArgs e)
        {
            
            if (advancedOptions.Visible)
            {
                advancedOptions.Visible = false;
            }
            else
            {
                advancedOptions.Visible = true;
            }
        }
        delegate void UpdateLogCallback(string message, string status, bool debugonly = false);
        public void UpdateLog(string message, string status, bool debugonly = false)
        {
            if (this.statusBox.InvokeRequired)
            {
                UpdateLogCallback d = new UpdateLogCallback(UpdateLog);
                this.Invoke(d, new object[] { message, status, debugonly });
            }
            else
            {
                debugwindow.updateLog(message, status);
                if (!debugonly)
                {
                    statusBox.SelectionStart = statusBox.Text.Length;
                    if (status == "error")
                    {
                        statusBox.SelectionColor = Color.Red;
                        message = "[Error] " + message;
                    }
                    else if (status == "warning")
                    {
                        statusBox.SelectionColor = Color.OrangeRed;
                        message = "[Warning] " + message;
                    }
                    else if (status == "info")
                    {
                        statusBox.SelectionColor = Color.Black;
                        message = "[Info] " + message;
                    }
                    else if (status == "done")
                    {
                        statusBox.SelectionColor = Color.DarkGreen;
                        message = "[Success] " + message;
                    }
                    else
                    {
                        statusBox.SelectionColor = Color.Black;
                    }
                    if (statusBox.Text == "")
                    {
                        //statusLog.Text = message;
                        statusBox.AppendText(message);
                    }
                    else
                    {
                        if (message == "LINEBREAK")
                        {
                            statusBox.AppendText(System.Environment.NewLine + "---------------------------------------------------------------------------------------------------");
                        }
                        else
                        {
                            statusBox.AppendText(System.Environment.NewLine + message);
                        }
                    }
                    statusBox.SelectionStart = statusBox.Text.Length;
                    statusBox.ScrollToCaret();
                }
            }
        }
        delegate void UpdateConvertDataCallback(string frames, string curFrame);
        public void UpdateConvertData(string frames, string curFrame)
        {
            if (this.statusBox.InvokeRequired)
            {
                UpdateConvertDataCallback d = new UpdateConvertDataCallback(UpdateConvertData);
                this.Invoke(d, new object[] { frames, curFrame });
            }
            else
            {
                fps.Text = frames;
                remaining.Text = curFrame;
            }
        }
        private bool checkCacheFolder()
        {
            try
            {
                string videos = Path.Combine(curpath, videoFolder);
                if (!Directory.Exists(videos))
                {
                    Directory.CreateDirectory(videos);
                    UpdateLog("Created '" + videoFolder + "' folder", "info");
                }
                return true;
            }
            catch (Exception e)
            {
                UpdateLog("Could not create cache folder! '" + e.ToString() + "'", "error");
            }
            return false;
        }
        public string GetSafeFilename(string filename)
        {
            return string.Join("", filename.Split(Path.GetInvalidFileNameChars()));
        }
        public string getConvertOptions(int pos, string s)
        {
            string[] words = s.Split('|');
            return words[pos];
        }
        public string createConvertOptions()
        {
            string output = "";
            if (deleteOriginal.Checked)
            {
                output += "1|";
            }
            else
            {
                output += "0|";
            }
            if (originalDir.Checked)
            {
                output += "1|";
            }
            else
            {
                output += "0|";
            }
            if (sampleClip.Checked)
            {
                output += "1|";
            }
            else
            {
                output += "0|";
            }
            output += offsetInput.Text + "|";
            if (enableAudioMap.Checked)
            {
                output += audioMap.Value.ToString();
            }
            else
            {
                // Try to find english 5.1 and auto choose that
                List<string> englishAudioData = new List<string>();
                string audioStream = "1";
                bool found = false;
                if (audioData.Count > 1)
                {
                    UpdateLog("Found multiple audio streams", "warning", true);
                    foreach (string a in audioData)
                    {
                        if (a.Contains("(eng)"))
                        {
                            englishAudioData.Add(a);
                            found = true;
                        }
                    }
                    if (!found)
                    {
                        UpdateLog("English audio not found!", "error", true);
                    }
                }
                if (englishAudioData.Count > 0)
                {
                    if (englishAudioData.Count > 1)
                    {
                        found = false;
                        UpdateLog("Found multiple english streams", "warning");
                        foreach (string a in audioData)
                        {
                            if (a.Contains("DTS"))
                            {
                                audioStream = a.Substring(a.IndexOf("Stream #0:") + ("Stream #0:").Length, 1);
                                UpdateLog("Found DTS English stream at ID " + audioStream + ": " + a, "info", true);
                                found = true;
                                break;
                            }
                            if (a.Contains("5.1"))
                            {
                                audioStream = a.Substring(a.IndexOf("Stream #0:") + ("Stream #0:").Length, 1);
                                UpdateLog("Found 5.1 English stream at ID " + audioStream + ": " + a, "info", true);
                                found = true;
                                break;
                            }
                        }
                        if (!found) // no surround sound found, lets default to the first one
                        {
                            UpdateLog("No english surround sound found, defaulting to: " + englishAudioData[0], "warning", true);
                            audioStream = englishAudioData[0].Substring(englishAudioData[0].IndexOf("Stream #0:") + ("Stream #0:").Length, 1);
                        }
                    }
                    else
                    {
                        audioStream = englishAudioData[0].Substring(englishAudioData[0].IndexOf("Stream #0:") + ("Stream #0:").Length, 1);
                        UpdateLog("Only 1 english stream found at MAP: " + audioStream, "info", true);
                    }
                }
                englishAudioData.Clear();
                output += audioStream;
            }
            //UpdateLog("Video Config: " + output, "warning");
            return output;
        }
        delegate void isPs4ConvertingCallback();
        private bool isPs4Converting(string _convert)
        {
            if (_convert == "PS4" || _convert == "PS4 alt")
            {
                return true;
            }
            return false;
        }
        private void ResetForm()
        {
            //tabber.Enabled = true;
            convertVideos.Visible = true;
            cancelConvert.Visible = false;
            progressBar1.Value = 0;
            removeSelected.Enabled = false;
            isRunning = false;
            procId1 = 0;
        }
        private bool checkFfmpeg()
        {
            if (!File.Exists("ffmpeg.exe"))
            {
                string add = "";
                UpdateLog("ffmpeg.exe is missing" + add, "error");
                return false;
            }
            return true;
        }



        // YouTube Downloader
        private void btn_paste(object sender, EventArgs e)
        {
            youtube.Text = Clipboard.GetText();
        }
        private void btn_pasteGo(object sender, EventArgs e)
        {
            youtube.Text = Clipboard.GetText();
            convertVideo.PerformClick();
        }
        private void btn_downloadVideo(object sender, EventArgs e)
        {
            string args = "";
            string playList = "";
            string addargs = "";
            string vidQuality = "";
            string saveTemplate = "-o \"" + videoFolder2 + "\\%(title)s.mp4\"";
            downOption = downloadOption.SelectedItem.ToString();
            string qOption = videoOption.SelectedItem.ToString();
            decimal playlistS = playlistIndexS.Value;
            decimal playlistE = playlistIndexE.Value;
            if (template.Text == "Playlist")
            {
                saveTemplate = "-o \"" + videoFolder2 + "\\%(playlist)s\\%(playlist_index)s - %(title)s.mp4\"";
                if (playlistS != 0)
                {
                    playList = "--playlist-start " + playlistS;
                }
                if (playlistE != 0)
                {
                    playList = playList + " --playlist-end " + playlistE;
                }
                if (newtoold.Checked)
                {
                    addargs = "--playlist-reverse";
                }
            }
            if (qOption == "Highest quality available (4k)")
            {
                vidQuality = " -f bestvideo+bestaudio/best";
            }
            else if (qOption == "Highest quality mp4 (4k)")
            {
                vidQuality = " -f bestvideo[ext!=webm]‌​+bestaudio[ext!=webm]‌​/bestvideo+bestaudio/best[ext!=webm]/best";
            }
            else if (qOption == "Mid quality mp4")
            {
                vidQuality = " -f 18/mp4/best";
            }
            else if (qOption == "Best quality mp4")
            {
                vidQuality = " -f mp4/best";
            }
            if (downOption == "Video + mp3")
            {
                extractAudio = true;
                args = "-i " + saveTemplate + vidQuality + " --no-part --restrict-filenames " + addargs + " " + playList + " --extract-audio --audio-format mp3 --keep-video \"" + youtube.Text.Trim() + "\"";
            }
            else if (downOption == "mp3")
            {
                extractAudio = true;
                args = "-i " + saveTemplate + vidQuality + " --no-part --restrict-filenames " + addargs + " " + playList + " --extract-audio --audio-format mp3 \"" + youtube.Text.Trim() + "\"";
            }
            else
            {
                args = "-i " + saveTemplate + vidQuality + " --no-part --restrict-filenames " + addargs + " " + playList + " \"" + youtube.Text.Trim() + "\"";
            }

            startDownload(args, youtube.Text.Trim());
            //UpdateLog2("Command: " + args, "info");
            if (!skipQueue)
            {
                File.AppendAllText(@queueFile, args + Environment.NewLine);
                File.AppendAllText(@logFile, "Downloading: " + youtube.Text + Environment.NewLine);
                youtube.Text = "";
            }
        }
        private void startDownload(string args, string URL)
        {
            if (isRunning2)
            {
                UpdateLog2("Added to queue: " + URL, "info");
                downloadQueue.Add(args);
                pendingDownloads.Text = "Queue: " + downloadQueue.Count;
            }
            else
            {
                UpdateLog2("Starting download from: " + URL, "info");
                isRunning2 = true;
                //convertVideo.Visible = false;
                cancelDownload.Visible = true;
                doUpdate.Enabled = false;
                bw2.RunWorkerAsync(args);
            }
        }
        private void btn_cancelDownload(object sender, EventArgs e)
        {
            if (isRunning2)
            {
                convertCanceled2 = true;
                bw2.CancelAsync();
                bw2.Dispose();
                Process p = Process.GetProcessById(procId2);
                p.Kill();
                procId2 = 0;
                System.Threading.Thread.Sleep(2000);
                File.Delete(fullDownloadFile);
            }
        }
        private void bw2_DoDownload(object sender, DoWorkEventArgs e)
        {
            Process pro = new Process();
            pro.StartInfo.RedirectStandardOutput = true;
            pro.StartInfo.RedirectStandardError = true;
            pro.StartInfo.UseShellExecute = false;
            pro.StartInfo.CreateNoWindow = true;
            pro.EnableRaisingEvents = true;
            if (!runUpdate)
            {
                pro.OutputDataReceived += new DataReceivedEventHandler(ParseDownloadData);
                pro.ErrorDataReceived += new DataReceivedEventHandler(ParseDownloadData);
            }
            else
            {
                pro.OutputDataReceived += new DataReceivedEventHandler(ParseUpdateData);
                pro.ErrorDataReceived += new DataReceivedEventHandler(ParseUpdateData);
            }
            pro.StartInfo.FileName = "youtube-dl.exe";
            pro.StartInfo.Arguments = (string)e.Argument;


            try
            {
                pro.Start();
                procId2 = pro.Id;
                pro.BeginOutputReadLine();
                pro.BeginErrorReadLine();
                pro.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            if (bw2.CancellationPending) e.Cancel = true;
        }
        void bw2_DownloadChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage > 100)
            {
                progressBar2.Value = 100;
                //TaskbarManager.Instance.SetProgressValue(100, 100);
                doTaskbarProgress(100,1);
            }
            else if (e.ProgressPercentage < 0)
            {
                progressBar2.Value = 0;
                //TaskbarManager.Instance.SetProgressValue(0, 100);
                doTaskbarProgress(0, 1);
            }
            else
            {
                progressBar2.Value = e.ProgressPercentage;
                //TaskbarManager.Instance.SetProgressValue(e.ProgressPercentage, 100);
                doTaskbarProgress(e.ProgressPercentage, 1);
            }
        }
        void bw2_DownloadCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.NoProgress);
            if (convertCanceled2)
            {
                UpdateLog("Download Canceled", "info");
                convertCanceled2 = false;
                ResetForm2();
            }
            else
            {
                if ((e.Cancelled == true))
                {
                    UpdateLog2("Download Canceled", "info");
                }
                else if (!(e.Error == null))
                {
                    UpdateLog2("Download Error: " + e.Error.Message, "error");
                }
                else
                {
                    if (runUpdate)
                    {
                        runUpdate = false;
                        convertVideo.Enabled = true;
                        doUpdate.Enabled = true;
                        if (hasError2)
                        {
                            UpdateLog2("Downloader is up to date", "done");
                            hasError2 = false;
                        }
                        else
                        {
                            UpdateLog2("Downloader has been updated", "done");
                        }
                    }
                    else
                    {
                        if (hasError2)
                        {
                            UpdateLog2("We had errors", "done");
                            hasError2 = false;
                        }
                        else
                        {
                            if (extractAudio)
                            {
                                if (downOption == "Video + mp3")
                                {
                                    UpdateLog2("Video downloaded and audio extracted", "done");
                                }
                                else
                                {
                                    UpdateLog2("Audio downloaded", "done");
                                }
                            }
                            else
                            {
                                UpdateLog2("Video downloaded", "done");
                            }
                        }

                        //UpdateLog("Video converted and stored in '" + videoFolder + "'", "done");
                    }
                }
                UpdateLog2("LINEBREAK", "");
                ResetForm2();
            }
            if (downloadQueue.Count > 0)
            {
                string queArgs = downloadQueue[0];
                removeFromQueue();
                downloadQueue.RemoveAt(0);
                pendingDownloads.Text = "Queue: " + downloadQueue.Count;
                isRunning2 = true;
                //convertVideo.Visible = false;
                cancelDownload.Visible = true;
                doUpdate.Enabled = false;
                bw2.RunWorkerAsync(queArgs);
            }
            else
            {
                File.Delete(@queueFile);
                File.AppendAllText(@logFile, "================ Downloading Done ===============" + Environment.NewLine + Environment.NewLine);
            }
        }
        public void ParseUpdateData(object sender, DataReceivedEventArgs e)
        {
            // Do some check on here to get the final ouput video name?
            if (!String.IsNullOrEmpty(e.Data))
            {
                if (e.Data.Contains("Updating to version"))
                {
                    UpdateLog2(e.Data, "info");
                }
                if (e.Data.Contains("youtube-dl is up-to-date"))
                {
                    hasError2 = true;
                }
                //UpdateLog2(e.Data, "info");
            }
        }
        public void ParseDownloadData(object sender, DataReceivedEventArgs e)
        {
            // Do some check on here to get the final ouput video name?
            if (!String.IsNullOrEmpty(e.Data))
            {
                if (e.Data.Contains("[download]"))
                {
                    if (e.Data.Contains("Destination:"))
                    {
                        string location = e.Data.Substring(e.Data.IndexOf("[download] Destination: ") + ("[download] Destination: ").Length).Trim();
                        UpdateLog2("Downloading to: " + location, "info");
                        fullDownloadFile = location;
                    }
                    if (e.Data.Contains("% of "))
                    {
                        string percentTmp = e.Data.Substring(e.Data.IndexOf("[download] ") + ("[download] ").Length, e.Data.IndexOf("%") - ("[download] ").Length).Trim();
                        int percentComplete = Convert.ToInt32(Math.Round(Convert.ToDouble(percentTmp)));
                        bw2.ReportProgress(percentComplete);
                    }
                    else if (e.Data.Contains(" has already been downloaded"))
                    {
                        UpdateLog2("Video has already been downloaded", "done");
                    }
                }
                if (e.Data.Contains("[ffmpeg]"))
                {
                    if (e.Data.Contains(".mp3"))
                    {
                        string location = e.Data.Substring(e.Data.IndexOf("[ffmpeg] Destination: ") + ("[ffmpeg] Destination: ").Length).Trim();
                        UpdateLog2("Extracting audio to: " + location, "info");
                    }
                }
                if (e.Data.Contains("ERROR:"))
                {
                    hasError2 = true;
                    string error = e.Data.Replace("ERROR: ", "");
                    UpdateLog2(error, "error");
                }
                //UpdateLog2(e.Data, "info");
            }
        }
        delegate void UpdateLogCallback2(string message, string status, bool debugonly = false);
        public void UpdateLog2(string message, string status, bool debugonly = false)
        {
            if (this.statusBox2.InvokeRequired)
            {
                UpdateLogCallback2 d = new UpdateLogCallback2(UpdateLog2);
                this.Invoke(d, new object[] { message, status, debugonly });
            }
            else
            {
                debugwindow.updateLog(message, status);
                if (!debugonly)
                {
                    statusBox2.SelectionStart = statusBox2.Text.Length;
                    if (status == "error")
                    {
                        statusBox2.SelectionColor = Color.Red;
                        message = "[Error] " + message;
                    }
                    else if (status == "warning")
                    {
                        statusBox2.SelectionColor = Color.OrangeRed;
                        message = "[Warning] " + message;
                    }
                    else if (status == "info")
                    {
                        statusBox2.SelectionColor = Color.Black;
                        message = "[Info] " + message;
                    }
                    else if (status == "done")
                    {
                        statusBox2.SelectionColor = Color.DarkGreen;
                        message = "[Success] " + message;
                    }
                    else
                    {
                        statusBox2.SelectionColor = Color.Black;
                    }
                    if (statusBox2.Text == "")
                    {
                        //statusLog.Text = message;
                        statusBox2.AppendText(message);
                    }
                    else
                    {
                        if (message == "LINEBREAK")
                        {
                            statusBox2.AppendText(System.Environment.NewLine + "---------------------------------------------------------------------------------------------------");
                        }
                        else
                        {
                            statusBox2.AppendText(System.Environment.NewLine + message);
                        }
                    }
                    statusBox2.SelectionStart = statusBox2.Text.Length;
                    statusBox2.ScrollToCaret();
                }
            }
        }
        private void doUpdate_Click(object sender, EventArgs e)
        {
            runUpdate = true;
            convertVideo.Enabled = false;
            doUpdate.Enabled = false;
            bw2.RunWorkerAsync("-U");
        }
        private void checkUrlForPlaylist(object sender, EventArgs e)
        {
            if (checkFolder())
            {
                string videoPath = youtube.Text.Trim();
                if (videoPath.Length != 0)
                {
                    if (videoPath.Contains("&index") && videoPath.Contains("youtube.com") && removeIndex.Checked)
                    {
                        Regex YoutubeVideoRegex = new Regex(@"(?:(.*)v(/|=)|(.*/)?)([a-zA-Z0-9-_]+)", RegexOptions.IgnoreCase);
                        Match youtubeMatch = YoutubeVideoRegex.Match(videoPath);

                        if (youtubeMatch.Success)
                        {
                            videoPath = "https://www.youtube.com/watch?v=" + youtubeMatch.Groups[4].Value;
                            youtube.Text = videoPath;
                        }
                        else
                        {
                            videoPath = videoPath.Remove(videoPath.IndexOf("&index"));
                            videoPath = videoPath.Remove(videoPath.IndexOf("&list"));
                            youtube.Text = videoPath;
                        }
                    }
                    bool isplaylist = false;
                    if (videoPath.Contains("youtube.com/channel") || videoPath.Contains("youtube.com/user") || videoPath.Contains("playlist"))
                    {
                        isplaylist = true;
                    }

                    if (isplaylist)
                    {
                        int index = template.FindString("Playlist");
                        template.SelectedIndex = index;
                    }
                    else
                    {
                        int index = template.FindString("Default");
                        template.SelectedIndex = index;
                    }
                    convertVideo.Enabled = true;
                    convertVideo.BackColor = Color.Green;
                }
                else
                {
                    convertVideo.Enabled = false;
                    convertVideo.BackColor = Color.LightGray;
                }
            }
            else
            {
                convertVideo.Enabled = false;
                convertVideo.BackColor = Color.LightGray;
            }
        }
        private void openDownloadLocation(object sender, EventArgs e)
        {
            string videos = Path.Combine(curpath, videoFolder2);
            Process.Start("explorer.exe", @videos);
        }
        private void downloadQueueFile()
        {
            string[] lines = File.ReadAllLines(queueFile);
            skipQueue = true;
            foreach (string line in lines)
            {
                if (line.Trim().Length > 0)
                {
                    args = line.Trim();
                    string[] tokens = args.Split(' ');
                    string URL = tokens[tokens.Length - 1].Replace("\"", "");
                    if (URL.Length > 0)
                    {
                        startDownload(args, URL);
                    }
                }
            }
            skipQueue = false;
        }
        private void removeFromQueue()
        {
            if (File.Exists(queueFile))
            {
                System.IO.File.WriteAllLines(@queueFile, downloadQueue);
            }
        }
        private void btn_downloadFile(object sender, EventArgs e)
        {
            if (File.Exists(downFile))
            {
                string[] lines = File.ReadAllLines(downFile);
                foreach (string line in lines)
                {
                    if (line.Trim().Length > 0)
                    {
                        youtube.Text = line.Trim();
                        convertVideo.PerformClick();
                    }
                }
                File.Delete(@downFile);
            }
            else
            {
                UpdateLog2("cache/import.txt could not be found", "warning");
            }
        }
        private bool checkFolder()
        {
            try
            {
                string videos = Path.Combine(curpath, videoFolder2);
                if (!Directory.Exists(videos))
                {
                    Directory.CreateDirectory(videos);
                    UpdateLog2("Created '" + videoFolder2 + "' folder", "info");
                }
                return true;
            }
            catch (Exception e)
            {
                UpdateLog2("Could not create video folder! '" + e.ToString() + "'", "error");
            }
            return false;
        }
        private void ResetForm2()
        {
            progressBar2.Value = 0;
            //downOption = "";
            //youtube.Text = "";
            hasError2 = false;
            //extractAudio = false;
            //convertVideo.Visible = true;
            cancelDownload.Visible = false;
            isRunning2 = false;
            convertCanceled2 = false;
            doUpdate.Enabled = true;
            procId2 = 0;
        }



        // Video Joiner
        private void btn_joinVideo(object sender, EventArgs e)
        {
            if (createCache() && checkFfmpeg())
            {
                if (joinVids.Rows.Count > 0)
                {
                    string fileOut = "";
                    string firstFile = joinVids.Rows[0].Cells[1].Value.ToString();
                    string firstExt = Path.GetExtension(firstFile);
                    string outFile = joinOutName.Text;
                    foreach (DataGridViewRow row in joinVids.Rows)
                    {
                        fileOut += "file '" + row.Cells[1].Value.ToString() + "'" + System.Environment.NewLine;
                    }

                    System.IO.File.WriteAllText(Path.Combine(curpath, cacheFolder) + "\\mylist.txt", fileOut);

                    args = "-f concat -i \"" + Path.Combine(curpath, cacheFolder) + "\\mylist.txt\" -c copy \"" + videoFolder + "\\" + outFile +  firstExt + "\"";

                    removeJoin.Enabled = false;
                    joinVideos.Enabled = false;
                    System.Diagnostics.Process.Start("ffmpeg.exe", args);
                    joinVideos.Enabled = true;
                    removeJoin.Enabled = true;

                    joinVids.Rows.Clear();
                    joinVids.Refresh();
                }
            }
        }
        private void btn_removeJoin(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in joinVids.SelectedRows)
            {
                joinVids.Rows.Remove(r);
            }
        }
        private void btnUpJoin_Click(object sender, EventArgs e)
        {
            DataGridView grid = joinVids;
            try
            {
                int totalRows = grid.Rows.Count;
                int idx = grid.SelectedCells[0].OwningRow.Index;
                if (idx == 0 || (isRunning && idx == 1))
                    return;
                int col = grid.SelectedCells[0].OwningColumn.Index;
                DataGridViewRowCollection rows = grid.Rows;
                DataGridViewRow row = rows[idx];
                rows.Remove(row);
                rows.Insert(idx - 1, row);
                grid.ClearSelection();
                grid.Rows[idx - 1].Cells[col].Selected = true;

            }
            catch { }
        }
        private void btnDownJoin_Click(object sender, EventArgs e)
        {

            DataGridView grid = joinVids;
            try
            {
                int totalRows = grid.Rows.Count;
                int idx = grid.SelectedCells[0].OwningRow.Index;
                if (idx == totalRows - 1 || (isRunning && idx == 0))
                    return;
                int col = grid.SelectedCells[0].OwningColumn.Index;
                DataGridViewRowCollection rows = grid.Rows;
                DataGridViewRow row = rows[idx];
                rows.Remove(row);
                rows.Insert(idx + 1, row);
                grid.ClearSelection();
                grid.Rows[idx + 1].Cells[col].Selected = true;
            }
            catch { }
        }
        private void btnTopJoin_Click(object sender, EventArgs e)
        {
            DataGridView grid = joinVids;
            try
            {
                int upIndex = 0;
                int totalRows = grid.Rows.Count;
                int idx = grid.SelectedCells[0].OwningRow.Index;
                if (idx == 0 || (isRunning && idx == 1))
                    return;
                int col = grid.SelectedCells[0].OwningColumn.Index;
                DataGridViewRowCollection rows = grid.Rows;
                DataGridViewRow row = rows[idx];
                rows.Remove(row);

                if (isRunning)
                {
                    upIndex = 1;
                }

                rows.Insert(upIndex, row);
                grid.ClearSelection();
                grid.Rows[upIndex].Cells[col].Selected = true;
            }
            catch { }
        }
        private void btnBottomJoin_Click(object sender, EventArgs e)
        {
            DataGridView grid = joinVids;
            try
            {
                int totalRows = grid.Rows.Count;
                int idx = grid.SelectedCells[0].OwningRow.Index;
                if (idx == totalRows - 1 || (isRunning && idx == 0))
                    return;
                int col = grid.SelectedCells[0].OwningColumn.Index;
                DataGridViewRowCollection rows = grid.Rows;
                DataGridViewRow row = rows[idx];
                rows.Remove(row);
                rows.Insert(totalRows - 1, row);
                grid.ClearSelection();
                grid.Rows[totalRows - 1].Cells[col].Selected = true;
            }
            catch { }
        }
        private void video_dragDrop2(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] temp = (string[])e.Data.GetData(DataFormats.FileDrop);
                files = new List<string>(temp);

                if (files.Count > 0)
                {
                    foreach (string f in files)
                    {
                        onlyfile = Path.GetFileName(f);
                        joinVids.Rows.Add(new object[] { onlyfile, f });
                    }
                    joinVideos.Enabled = true;
                    removeJoin.Enabled = true;
                }
            }
        }
        private void video_dragEnter2(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        private bool createCache()
        {
            try
            {
                string cache = Path.Combine(curpath, cacheFolder);
                if (!Directory.Exists(cache))
                {
                    Directory.CreateDirectory(cache);
                }
                return true;
            }
            catch (Exception e)
            {
                //UpdateLog("Could not create cache folder!", "error");
                string stopwarning = e.ToString();
            }
            return false;
        }




        // Various Global
        private void Form1_Closing(object sender, FormClosingEventArgs e)
        {
            // Determine if we are converting or downloading
            if (isRunning || isRunning2)
            {
                // Display a MsgBox asking the user to save changes or abort. 
                if (MessageBox.Show("There is still a running process\nAre you sure you want to quit?", "Spiffy Video Converter",
                   MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    // Cancel the Closing event from closing the form.
                    if (isRunning)
                    {
                        bw.CancelAsync();
                        bw.Dispose();
                        bw = null;
                        Process p = Process.GetProcessById(procId1);
                        p.Kill();
                        System.Threading.Thread.Sleep(2000);
                        File.Delete(fullSaveFile);
                    }
                    if (isRunning2)
                    {
                        bw2.CancelAsync();
                        bw2.Dispose();
                        bw2 = null;
                        Process p = Process.GetProcessById(procId2);
                        p.Kill();
                        System.Threading.Thread.Sleep(2000);
                        File.Delete(fullDownloadFile);
                    }
                    GC.Collect();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }
        private void updateLatest(object sender, EventArgs e)
        {
            if (Directory.Exists(updatepath))
            {
                if (File.Exists(updatepath + appfile))
                {
                    if (Path.GetFileName(System.Reflection.Assembly.GetEntryAssembly().Location) != appfile + ".OLD")
                    {
                        System.IO.File.Move(appfile, appfile + ".OLD");
                    }
                    else
                    {
                        File.Delete(@appfile);
                    }
                    File.Copy(@updatepath + appfile, @curpath + slash + appfile);
                    MessageBox.Show("App will be updated on next run");
                }
            }
        }
        void log_FormClosed(object sender, FormClosingEventArgs e)
        {
            debug.Checked = false;
            debug.Enabled = true;
        }
        private void toggleDebug(object sender, EventArgs e)
        {
            if (debug.Checked)
            {
                debugwindow.Show();
                debug.Enabled = false;
            }
        }
        private void loadSpiffyHacks(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://spiffyhacks.com");
        }
        delegate void doTaskbarProgressCallback(int _percent, int _tab);
        private void doTaskbarProgress(int _percent, int _tab)
        {
            if (this.statusBox.InvokeRequired)
            {
                doTaskbarProgressCallback d = new doTaskbarProgressCallback(doTaskbarProgress);
                this.Invoke(d, new object[] { _percent, _tab });
            }
            else
            {
                if (isRunning && isRunning2) // Only limit progress bar per tab if both converting and downloading are running
                {
                    if (tabber.SelectedIndex == _tab)
                    {
                        TaskbarManager.Instance.SetProgressValue(_percent, 100);
                    }
                }
                else
                {
                    TaskbarManager.Instance.SetProgressValue(_percent, 100);
                    //UpdateLog2("Set Percent: " + _percent, "info");
                }
            }
        }

        private void btn_downloadFile()
        {

        }
    }
}
