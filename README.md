## About ##

This is a program I made personally for me with my own conversions that I use often.  If you have a suggestion, let me know and I can look into adding it, but most likely will not add any extensive options such as bitrate.  This program is meant to be simple to use with the least amount of options as possible to do specific tasks.

## Features ##

You can mainly convert videos into mp4 videos for a few different devices. It has an option to download videos from the web via the youtube-dl app and to combine videos as well.

## Conversion Profiles ##

**RC Plane MP4** - This option is to convert my videos from my 808 keychain camera to good quality mp4 videos.  The flip video option is mainly for these videos in case the camera is mounted upside down on the plane.

**Quad Copter** - I made this option to convert my JJRC H6C Quad Copter videos into mp4.

**Xbox AVI** - This option will convert the video into a playable AVI video on the original xbox via XBMC.

**Xbox MP4 Animated / General** - The animated one will set an animated flag to optimize animated movies such as Family Guy etc.  The general one is for normal shows with live people or 3d animation movies.  The video is playable on an original Xbox via XBMC.

**Blu-ray Convert** - I created this profile to keep the highest quality video while converting it into an MP4 video that works on my Samsung Smart TV and to try keeping the filesize to a minimum. MKV to this option works well. This does keep the 5.1 surround sound.

**Blu-ray Copy** - This simply keeps the video in the original format and copies it to an MP4 format.  This method is best to keep the full quality, but convert it to an MP4 format instead.  This method does not always work for my Samsung Smart TV so I usually do the convert one.

**Blu-ray Cabin** - I created this one to convert videos into a playable format on my Philips TV at our cabin.  I don't remember the model or anything, but it can play movies from USB and is very particular about how they are encoded so I created this option so they are in the correct format.

**Innotab** - This was removed.  I would suggest getting the [Innotab Manager Software](http://spiffyhacks.com/thread-483.html) to convert videos for the Innotab.

**Canon Camcorder MTS** - This was to convert my Canon VIXIA HFG20 MTS videos to good quality mp4. This sets the rate at 60fps, 720 resolution and deinterlaces the video to create a high quality, smooth video. I set this as 720 instead of the 1080 because my TV will not stream the 1080 at 60fps.

**Straight Convert to mp4** - You can try this to simply convert to mp4 with no special commands or anything else if nothing else works.


I also put in code to the conversion to convert my security camera footage to mp4 from a dat file I believe.... Not 100% on that since I don't do it very often, but seems to work well. I don't remember which profile does it or if they all do.